#!/bin/bash


########################################################################
#                                                                      #
#                       P B B R I G H T N E S S                        #
#                                                                      #
#            SCRIPT TO CHANGE THE BRIGHTNESS OF THEW SCREEN            #
#                                                                      #
#                      Copyright 2017 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                       Version 1.0 / 20.11.2017                       #
#                                                                      #
########################################################################
#
#  1 - Check if the software package 'xbindkeys' is installed on your
#      system or install it with: sudo apt-get install xbindkeys
#
#  2 - Create a default configuration file in your home directory with
#      the command: xbindkeys --defaults > $HOME/.xbindkeysrc
#
#  3 - Edit the new configuration file in your home directory and add
#      the following shortcut definitions:
#
#          "/home/username/pbbrightness/brightness.sh up"
#          Control+Mod2+Up
#
#          "/home/username/pbbrightness/brightness.sh down"
#          Control+Mod2+Down
#
#          "/home/username/pbbrightness/brightness.sh default"
#          Control+Mod2+Home
#
#          "/home/username/pbbrightness/brightness.sh minimum"
#          Control+Mod2+End
#
#  4 - Save the configuration file and reload 'xbindkeys' to apply the
#      changes made: xbindkeys -p
#
#  5 - Display all the active shortcutsd: xbindkeys -s
#
#  6 - Make this script executable: sudo chmod +x brightness.sh
#
#  7 - Customize the configuration section of the script if necessary.
#
#  8 - Use the specified keys to change the brightness of the screen.
#
# ======================================================================


########################################################################
########################################################################
##############   C O N F I G U R A T I O N   B E G I N   ###############
########################################################################
########################################################################


# ======================================================================
# Please specify the output signal for the used screen manually and
# enter 'DP1', 'HDMI1', 'HDMI2', or 'VGA' as the output variable or let
# the system use the first actual active monitor.
# ======================================================================
# OUTPUT="DP1"
OUTPUT=$(xrandr --listactivemonitors | grep "0:" | tr -s " " | cut -d " " -f 5)


# ======================================================================
# Please specify the default screen brightness (1 - 10).
# ======================================================================
DEFAULT_BRIGHTNESS=7


# ======================================================================
# Please specify the minimum screen brightness (1 - 10).
# ======================================================================
MINIMUM_BRIGHTNESS=1


########################################################################
########################################################################
################   C O N F I G U R A T I O N   E N D   #################
########################################################################
########################################################################


# ======================================================================
# Clear the screen.
# ======================================================================
clear


# ======================================================================
# Check if no arguments were specified on the command line.
# ($# holds the number of arguments passed to the script)
# ======================================================================
if [ $# -eq 0 ]; then


  # ====================================================================
  # Use the default brightness.
  # ====================================================================
  ACTION=$DEFAULT_BRIGHTNESS


# ======================================================================
# There was an argument specified.
# ======================================================================
else


  # ====================================================================
  # Get the action from the command line.
  # ====================================================================
  ACTION=$1
fi


# ======================================================================
# Display the action command.
# ======================================================================
echo "Action: $ACTION"


# ======================================================================
# Get the actual brightness of the screen.
# ======================================================================
ACTUAL_BRIGHTNESS=$(xrandr --verbose | grep -m 1 "Brightness:" | grep -o '[0-9].*')
ACTUAL_BRIGHTNESS=$(echo "$ACTUAL_BRIGHTNESS*10" | bc)
ACTUAL_BRIGHTNESS=${ACTUAL_BRIGHTNESS%.*}


# ======================================================================
# Check if the screen brightness should be increased.
# ======================================================================
if [ "$ACTION" == "up" ]; then



  # ====================================================================
  # Check if the screen brightness is lower than 9.
  # ====================================================================
  if [ $ACTUAL_BRIGHTNESS -lt 9 ]; then


    # ==================================================================
    # Increase the screen brightness 1 step.
    # ==================================================================
    NEW_BRIGHTNESS=$((ACTUAL_BRIGHTNESS+1))


  # ====================================================================
  # The screen brightness is 9 or higher.
  # ====================================================================
  else


    # ==================================================================
    # Set the screen brightness to the maximum of 10.
    # ==================================================================
    NEW_BRIGHTNESS=10
  fi


# ======================================================================
# Check if the screen brightness should be decreased.
# ======================================================================
elif [ "$ACTION" == "down" ]; then


  # ====================================================================
  # Check if the screen brightness is higher than 1.
  # ====================================================================
  if [ $ACTUAL_BRIGHTNESS -gt 1 ]; then


    # ==================================================================
    # Decrease the screen brightness 1 step.
    # ==================================================================
    NEW_BRIGHTNESS=$((ACTUAL_BRIGHTNESS-1))


  # ====================================================================
  # The screen brightness is 1 or lower.
  # ====================================================================
  else


    # ==================================================================
    # Set the screen brightness to the minimum of 1.
    # ==================================================================
    NEW_BRIGHTNESS=1
  fi


# ======================================================================
# Check if the screen brightness should be set to the specified default
# value.
# ======================================================================
elif [ "$ACTION" == "default" ]; then


  # ====================================================================
  # Set the screen brightness to the specified default value.
  # ====================================================================
  NEW_BRIGHTNESS=$DEFAULT_BRIGHTNESS


# ======================================================================
# Check if the screen brightness should be set to the specified minimum
# value.
# ======================================================================
elif [ "$ACTION" == "minimum" ]; then


  # ====================================================================
  # Set the screen brightness to the default value.
  # ====================================================================
  NEW_BRIGHTNESS=$MINIMUM_BRIGHTNESS


# ======================================================================
# Check if the screen brightness was specified directly.
# ======================================================================
else


  # ====================================================================
  # Check if the screen brightness is correct: >=1 and <=10.
  # ====================================================================
  if [[ $ACTION -ge 1 && $ACTION -le 10 ]]; then


    # ==================================================================
    # Set the screen brightness to the specified value.
    # ==================================================================
    NEW_BRIGHTNESS=$ACTION


  # ====================================================================
  # The direct input of the screen brightness was wrong.
  # ====================================================================
  else


    # ==================================================================
    # Set the screen brightness to the default value.
    # ==================================================================
    NEW_BRIGHTNESS=$DEFAULT_BRIGHTNESS
  fi
fi


# ======================================================================
# Calculate the screen brightness in percent.
# ======================================================================
PERCENT_BRIGHTNESS=$(echo "scale=1; $NEW_BRIGHTNESS*10" | bc)


# ======================================================================
# Display the screen brightness in percent.
# ======================================================================
echo "New brightness: $PERCENT_BRIGHTNESS%"


# ======================================================================
# Check if the new screen brightness is lower than 1.
# ======================================================================
if [ $NEW_BRIGHTNESS -lt 1 ]; then


  # ====================================================================
  # Set the command brightness to the minimum of 0.1.
  # ====================================================================
  COMMAND_BRIGHTNESS="0.1"


# ======================================================================
# The new screen brightness is 1 or higher.
# ======================================================================
else 


  # ====================================================================
  # Calculate the command brightness.
  # ====================================================================
  COMMAND_BRIGHTNESS=$(echo "scale=1; $NEW_BRIGHTNESS/10" | bc)
fi


# ======================================================================
# Set the new command brightness for the specified screen.
# ======================================================================
xrandr --output "$OUTPUT" --brightness $COMMAND_BRIGHTNESS
