# PBbrightness Script - README #
---

### Overview ###

The **PBbrightness** script is a small Bash file which is used to set/change the brightness of a screen in a graphical desktop environment like XFCE. In combination with the shortcut software package **xbindkeys** you can increase or decrease the screen brightness in 10% steps through shortcuts and set the screen brightness to a specified minimum or default value. This script was necessary because I did not find any normal possibility to change the screen brightness.

### Setup ###

* Install the necessary package: **sudo apt-get install xbindkeys**
* Create a default configuration file: **xbindkeys --defaults > $HOME/.xbindkeysrc**
* Edit the configuration file and add some shortcuts (see script header for more info).
* Save and reload the configuration: **xbindkeys -p**
* Check the available shortcuts: **xbindkeys -s**
* Copy the whole directory **pbbrightness** to your computer.
* Open the script **brightness.sh** in your favorite text editor and customize the configuration.
* Make the script executable: **chmod +x /path/to/script/pbbrightness/brightness.sh**
* Use the specified keys to change the brightness of the screen.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBbrightness** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
